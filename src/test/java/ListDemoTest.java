import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;


public class ListDemoTest {

    @Test
    public void sameSurnameListTest(){
        List<Human> expectedRes = new ArrayList<>();
        List<Human> inList = new ArrayList<>();
        Human inHuman = new Human("Ряполов","Генрик", "Олегович", 18);
        Human human1 = new Human("Ряполов","Виталий", "Олегович", 18);
        Human human2 = new Human("Иванов","Генрик", "Олегович", 18);
        Human human3 = new Human("Сидоров","Генрик", "Олегович", 18);
        Human human4 = new Human("Ряполов","Евгений", "Олегович", 18);
        expectedRes.add(human1);
        expectedRes.add(human4);
        inList.add(human1);
        inList.add(human2);
        inList.add(human3);
        inList.add(human4);
        List<Human> actualRes = ListDemo.sameSurnameList(inList, inHuman);
        assertEquals(expectedRes,actualRes);
    }
    @Test
    public void maxAgedListTest(){
        Set<Human> expectedRes = new HashSet<>();
        Set<Human> expectedRes2 = new HashSet<>();
        List<Human> inList = new ArrayList<>();
        List<Student> inList2 = new ArrayList<>();
        Human human1 = new Human("Ряполов","Виталий", "Олегович", 18);
        Student student = new Student("Иванов","Генрик", "Олегович", 19, "ИМИТ");
        Human human2 = new Human("Сидоров","Генрик", "Олегович", 18);
        Student student2 = new Student("Петров","Малахий", "Анатольевич", 25, "ИМИТ");
        Human human3 = new Human("Атаев","Генрик", "Олегович", 25);
        expectedRes.add(student2);
        expectedRes.add(human3);
        expectedRes2.add(student2);
        inList.add(human1);
        inList.add(student);
        inList.add(human2);
        inList.add(student2);
        inList.add(human3);
        inList2.add(student);
        inList2.add(student2);
        Set<Human> actualRes = ListDemo.maxAgedList(inList);
        Set<Student> actualRes2 = ListDemo.maxAgedList(inList2);
        assertEquals(expectedRes,actualRes);
        assertEquals(expectedRes2,actualRes2);
    }
}
