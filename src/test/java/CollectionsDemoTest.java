import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

public class CollectionsDemoTest {

    @Test
    public void symbolInStringTest(){
        String[] mass = { "xeon", "map", "baggage", "xll", "xcom", "raptor", ""};
        List<String> list = Arrays.asList(mass);
        char symbol = 'x';
        int res = CollectionsDemo.symbolInString(list,symbol);
        assertEquals(3,res);
    }

    @Test
    public void listWithoutHumanTest() throws CloneNotSupportedException {
        Human human1 = new Human("Ряполов","Генрик", "Олегович", 18);
        Human human2 = new Human("Латыпов","Генрик", "Олегович", 18);
        Human human3 = new Human("Буржуазов","Вильгельм", "Рюрикович", 18);
        Human human4 = new Human("Буржуазов","Александр", "Святославович", 18);
        List<Human> expectedRes = new ArrayList<>();
        List<Human> inList = new ArrayList<>();
        inList.add(human1);
        inList.add(human2);
        inList.add(human3);
        expectedRes.addAll(inList);
        inList.add(human4);
        List<Human> actualRes = CollectionsDemo.listWithoutHuman(inList,human4);
        assertEquals(expectedRes,actualRes);
    }

    @Test
    public void intersectionTest(){
        List<Set<Integer>> expectedRes = new ArrayList<>();
        List<Set<Integer>> inList = new ArrayList<>();
        Set<Integer> inSet = new HashSet<>();
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        Set<Integer> set3 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set2.add(1);
        set2.add(5);
        set2.add(8);
        set3.add(17);
        set3.add(14);
        inSet.add(1);
        inList.add(set1);
        inList.add(set2);
        inList.add(set3);
        expectedRes.add(set3);
        List<Set<Integer>> actualRes = CollectionsDemo.intersection(inList, inSet);
        assertEquals(expectedRes,actualRes);
    }

    @Test
    public void setOfCertainAgesTest()
    {
        Map<Integer, Human> inMap = new HashMap<>();
        Set<Integer> inSet = new HashSet<>();
        Set<Human> expectedRes = new HashSet<>();
        Human human1 = new Human("Ряполов","Генрик", "Олегович", 18);
        Human human2 = new Human("Латыпов","Генрик", "Олегович", 18);
        Human human3 = new Human("Буржуазов","Вильгельм", "Рюрикович", 18);
        Human human4 = new Human("Буржуазов","Александр", "Святославович", 18);
        inSet.add(1);
        inSet.add(12);
        inMap.put(1,human1);
        inMap.put(5,human2);
        inMap.put(7,human3);
        inMap.put(12,human4);
        expectedRes.add(human1);
        expectedRes.add(human4);
        Set<Human> actualRes = CollectionsDemo.setOfCertainAges(inMap,inSet);
        assertEquals(expectedRes, actualRes);
    }

    @Test
    public void setOfAgesNotLess18Test()
    {
        Map<Integer, Human> inMap = new HashMap<>();
        List<Integer> expectedRes = new ArrayList<>();
        Human human1 = new Human("Ряполов","Генрик", "Олегович", 18);
        Human human2 = new Human("Латыпов","Генрик", "Олегович", 15);
        Human human3 = new Human("Буржуазов","Вильгельм", "Рюрикович", 10);
        Human human4 = new Human("Буржуазов","Александр", "Святославович", 19);
        inMap.put(1,human1);
        inMap.put(5,human2);
        inMap.put(7,human3);
        inMap.put(12,human4);
        expectedRes.add(1);
        expectedRes.add(12);
        List<Integer> actualRes = CollectionsDemo.setOfAgesNotLess18(inMap);
        assertEquals(expectedRes, actualRes);
    }

    @Test
    public void mapBetweenIdsAgesTest()
    {
        Map<Integer, Human> inMap = new HashMap<>();
        Map<Integer, Integer> expectedRes = new HashMap<>();
        Human human1 = new Human("Ряполов","Генрик", "Олегович", 18);
        Human human2 = new Human("Латыпов","Генрик", "Олегович", 15);
        Human human3 = new Human("Буржуазов","Вильгельм", "Рюрикович", 10);
        Human human4 = new Human("Буржуазов","Александр", "Святославович", 19);
        inMap.put(1,human1);
        inMap.put(2,human2);
        inMap.put(3,human3);
        inMap.put(4,human4);
        expectedRes.put(1,18);
        expectedRes.put(2,15);
        expectedRes.put(3,10);
        expectedRes.put(4,19);
        Map<Integer, Integer> actualRes = CollectionsDemo.mapBetweenIdsAges(inMap);
        assertEquals(expectedRes, actualRes);
    }

    @Test
    public void mapBetweenAgesAndHumansTest()
    {
        Set<Human> inSet = new HashSet<>();
        Map<Integer, List<Human>> expectedRes = new HashMap<>();
        Human human1 = new Human("Ряполов","Генрик", "Олегович", 18);
        Human human2 = new Human("Латыпов","Генрик", "Олегович", 27);
        Human human3 = new Human("Буржуазов","Вильгельм", "Рюрикович", 18);
        Human human4 = new Human("Буржуазов","Александр", "Святославович", 27);
        List<Human> list1 = new ArrayList<>();
        List<Human> list2 = new ArrayList<>();
        list1.add(human1);
        list1.add(human3);
        list2.add(human2);
        list2.add(human4);
        inSet.add(human1);
        inSet.add(human2);
        inSet.add(human3);
        inSet.add(human4);
        expectedRes.put(18, list1);
        expectedRes.put(27, list2);
        Map<Integer, List<Human>> actualRes = CollectionsDemo.mapBetweenAgesAndHumans(inSet);
        Comparator<Human> comparator = (Comparator.comparing(Human::getSurname));
        expectedRes.get(18).sort(comparator);
        expectedRes.get(27).sort(comparator);
        actualRes.get(18).sort(comparator);
        actualRes.get(27).sort(comparator);
        assertEquals(expectedRes, actualRes);
    }





}
