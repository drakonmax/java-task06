import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListDemo {

    public static List<Human> sameSurnameList(List<Human> list, Human human)
    {
        List<Human> res = new ArrayList<>();
        String surname = human.getSurname();
        for (Human elem : list) {
            if(elem.getSurname().equals(surname))
                res.add(elem);
        }
        return res;
    }

    public static <T extends Human> Set<T> maxAgedList(List<T> list)
    {
        Set<T> res = new HashSet<>();
        int max=0;
        for (T elem : list) {
            int age = elem.getAge();
            if (age > max)
            {
                res.clear();
                max = age;
                res.add(elem);
            } else if (age == max)
                res.add(elem);
        }
        return res;
    }


}
