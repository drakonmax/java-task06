import java.util.*;

public class Human implements Cloneable{
    private String surname;
    private String name;
    private String lastname;
    private int age;

    public Human(String surname, String name, String lastname, int age) {
        this.surname = surname;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }




    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age)  {
        if (age<0) throw new IllegalArgumentException("Bad argument");
        else this.age = age;
    }

    @Override
    public Human clone() throws CloneNotSupportedException {
        return (Human) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(name, human.name) &&
                Objects.equals(lastname, human.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surname, name, lastname, age);
    }
}
