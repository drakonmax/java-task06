import java.util.*;

public class CollectionsDemo{

    public static int symbolInString(List<String> list, char symbol){
        int res = 0;
        for (String elem:list) {
            if (elem.length()!=0 && elem.charAt(0)==symbol)
                res++;
        }
        return res;
    }

    public static List<Human> listWithoutHuman(List<Human> list, Human human) throws CloneNotSupportedException {
        List<Human> res = new ArrayList<>();
        for (Human person: list) {
            if(!person.equals(human))
                res.add(person.clone());
        }
        return res;
    }

    public static List<Set<Integer>> intersection(List<Set<Integer>> list, Set<Integer> inSet) {
        List<Set<Integer>> res = new ArrayList<>();
        for (Set<Integer> elem : list) {
            Set<Integer> set = new HashSet<>(elem);
            set.retainAll(inSet);
            if (set.isEmpty())
                res.add(elem);
        }
        return res;
    }
    public static Set<Human> setOfCertainAges(Map<Integer, Human> map, Set<Integer> inSet)
    {
        Set<Human> res = new HashSet<>();
        for (Integer id : inSet){
            Human human = map.get(id);
            if (human != null)
                res.add(map.get(id));
        }
        return res;
    }

    public static List<Integer> setOfAgesNotLess18(Map<Integer, Human> map)
    {
        List<Integer> res = new ArrayList<>();
        for (Integer id : map.keySet()){
            if(map.get(id).getAge() >= 18)
            res.add(id);
        }
        return res;
    }

    public static Map<Integer, Integer> mapBetweenIdsAges(Map<Integer, Human> map)
    {
        Map<Integer, Integer> res = new HashMap<>(map.size());
        for (Integer id : map.keySet()){
            res.put(id, map.get(id).getAge());
        }
        return res;
    }

    public static Map<Integer, List<Human>> mapBetweenAgesAndHumans(Set<Human> inSet)
    {
        Map<Integer, List<Human>> res = new HashMap<>();

        for (Human elem : inSet) {
            int age = elem.getAge();
            if(res.containsKey(age))
                res.get(age).add(elem);
            else
                {
                    List <Human> keeper = new ArrayList<>();
                    keeper.add(elem);
                    res.put(elem.getAge(), keeper);
                }

        }
                return res;
        }
}
